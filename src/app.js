const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
require('dotenv').config();

const server = express();
const PORT = process.env.SERVER_PORT || 3000;

server.use(cors());
server.use(express.urlencoded({ extended: false }));
server.use(express.json());

if (process.env.ENVIRONMENT === 'dev') {
    server.use(morgan('dev'));
}

require('./routes/RouterExporter')(server);

server.listen(PORT);

module.exports = server;
