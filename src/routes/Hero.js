const express = require('express');
const { HeroModel } = require('../models/ModelExporter');
const notificationUtil = require('../config/notificationUtil');

const router = express.Router();

router.get('/', (req, res) => {
    HeroModel.getHeroesList()
        .then(heroes => {
            if (heroes.stack) {
                notificationUtil.doNotify('err', 'Erro ao Buscar Lista de Heróis');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro ao Buscar Lista de Heróis' });
            } else {
                notificationUtil.doNotify('success', 'Lista de Heróis Encontada');
                res.status(200).send(heroes);
            }
        })
        .catch(err => {
            if (err.stack) {
                notificationUtil.doNotify('err', 'Erro ao Buscar Lista de Heróis');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro ao Buscar Lista de Heróis' });
            } else {
                notificationUtil.doNotify('err', 'Erro Interno do Servidor');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro Interno do Servidor' });
            }
        });
});
router.get('/:id', (req, res) => {
    const { id } = req.params;
    HeroModel.getHero(id)
        .then(hero => {
            if (hero.stack) {
                notificationUtil.doNotify('err', 'Erro ao Buscar Herói');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro ao Buscar Herói' });
            } else {
                notificationUtil.doNotify(
                    'success',
                    `Herói Encontado: ${hero['person_name']}(${hero['hero_name']})`
                );
                res.status(200).send(hero);
            }
        })
        .catch(err => {
            if (err.stack) {
                notificationUtil.doNotify('err', 'Erro ao Buscar Herói');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro ao Buscar Herói' });
            } else {
                notificationUtil.doNotify('err', 'Erro Interno do Servidor');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro Interno do Servidor' });
            }
        });
});
router.post('/', (req, res) => {
    const { personName, heroName, image, brand, status, team } = req.body;
    HeroModel.newHero({ personName, heroName, image, brand, status, team })
        .then(newHeroId => {
            if (newHeroId.stack) {
                notificationUtil.doNotify('err', 'Erro ao Salvar Herói');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro ao Salvar Herói' });
            } else {
                notificationUtil.doNotify('success', `Novo Herói Salvo: ${heroName}`);
                res.status(200)
                    .send({ status: 'OK', msg: `Novo Herói Salvo: ${heroName}` });
            }
        }).catch(err => {
            if (err.stack) {
                notificationUtil.doNotify('err', 'Erro ao Salvar Herói');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro ao Salvar Herói' });
            } else {
                notificationUtil.doNotify('err', 'Erro Interno do Servidor');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro Interno do Servidor' });
            }
        });
});
router.delete('/', (req, res) => {
    const { id } = req.body;
    HeroModel.deleteHeroAndStatus(id)
        .then(deleteStatus => {
            if (deleteStatus.stack) {
                notificationUtil.doNotify('err', 'Erro ao Deletar Heróis e Status');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro ao Deletar Heróis e Status' });
            }
            notificationUtil.doNotify('success', 'Herói e Status Deletados');
            res.status(200)
                .send({ status: 'OK', msg: 'Herói e Status Deletados' });
        }).catch(err => {
            if (err.stack) {
                notificationUtil.doNotify('err', 'Erro ao Deletar Heróis e Status'); res.status(400)
                    .send({ status: 'ERROR', msg: 'Error Deleting Hero and Status' });
            } else {
                notificationUtil.doNotify('err', 'Erro Interno do Servidor');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro Interno do Servidor' });
            }

        });
});
module.exports = router;