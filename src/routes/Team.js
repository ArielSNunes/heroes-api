const express = require('express');
const router = express.Router();
const { TeamModel } = require('../models/ModelExporter');

router.get('/', (req, res) => {
    TeamModel.getTeams()
        .then(teamList => {
            if (!teamList || teamList.length <= 0)
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Error Retrieving Teams' });
            res.status(200).send(teamList);
        })
        .catch(err => {
            if (err)
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Error Retrieving Teams' });
            res.status(400)
                .send({ status: 'ERROR', msg: 'Internal Server Error' });
        });
});
router.get('/:id', (req, res) => {
    const { id } = req.params;
    TeamModel.getOneTeam(id)
        .then(team => {
            if (!team) return res.send(400)
                .send({ status: 'ERROR', msg: 'Error Retrieving Team' });
            return res.status(200).send(team);
        })
        .catch(err => {
            if (err)
                return res.status(400)
                    .send({ status: 'ERROR', msg: err });
            res.status(400)
                .send({ status: 'ERROR', msg: 'Internal Server Error' });
        });
});
router.post('/', (req, res) => {
    const { name } = req.body;
    TeamModel.registerTeam(name)
        .then(() => {
            res.status(200)
                .send({ status: 'OK', msg: 'New Team Saved: '.concat(name) });
        })
        .catch(err => {
            if (err)
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Error Saving new Team' });
            res.status(400)
                .send({ status: 'ERROR', msg: 'Internal Server Error' });
        });
});
router.put('/', (req, res) => {
    const { id, name } = req.body;
    TeamModel.editTeamName(id, name)
        .then(team => {
            if (!team)
                res.status(200)
                    .send({ status: 'WARNING', msg: 'Error Updating Team' });
            res.status(200)
                .send({ status: 'OK', msg: team });
        })
        .catch(err => {
            if (err)
                return res.status(400)
                    .send({ status: 'ERROR', msg: err });
            res.status(400)
                .send({ status: 'ERROR', msg: 'Internal Server Error' });
        });
});
router.delete('/', (req, res) => {
    const { id } = req.body;
    TeamModel.deleteTeam(id)
        .then(data => {
            if (data.stack)
                return res.status(200)
                    .send({ status: 'WARNING', msg: 'A Relationship Prevents this Team to be Deleted' });
            return res.status(200)
                .send({ status: 'OK', msg: 'Team Deleted' });
        })
        .catch(err => {
            if (err) return res.status(400)
                .send({ status: 'ERROR', msg: err });
            res.status(400)
                .send({ status: 'ERROR', msg: 'Internal Server Error' });
        });
})

module.exports = router;