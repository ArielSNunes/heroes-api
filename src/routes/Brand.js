const express = require('express');
const router = express.Router();
const { BrandModel } = require('../models/ModelExporter');
const notificationUtil = require('../config/notificationUtil');

router.get('/', (req, res) => {
    BrandModel.getBrands()
        .then(brands => {
            if (brands.stack) {
                notificationUtil.doNotify('err', 'Erro ao Buscar Lista de Marcas');
                res.status(400).send({ status: 'ERROR', msg: 'Erro ao Buscar Lista de Marcas' });
            } else {
                notificationUtil.doNotify('success', 'Lista de Marcas Encontradas');
                res.status(200).send(brands);
            }
        })
        .catch(err => {
            if (err.stack) {
                notificationUtil.doNotify('err', 'Erro ao Buscar Lista de Marcas');
                res.status(400).send({ status: 'ERROR', msg: 'Erro ao Buscar Lista de Marcas' });
            } else {
                notificationUtil.doNotify('err', 'Erro Interno do Servidor');
                res.status(400).send({ status: 'ERROR', msg: 'Erro Interno do Servidor' });
            }
        });
});

router.get('/:id', (req, res) => {
    const { params: { id } } = req;
    BrandModel.getOneBrand(id)
        .then(brand => {
            if (brand.stack) {
                notificationUtil.doNotify('err', 'Erro ao Buscar Marca');
                return res.send(400).send({ status: 'ERROR', msg: 'Erro ao Buscar Marca' });
            } else {
                notificationUtil.doNotify('success', 'Marca Encontrada');
                res.status(400).send(brand);
            }
        })
        .catch(err => {
            if (err.stack) {
                notificationUtil.doNotify('err', 'Erro ao Buscar Marca');
                res.status(400).send({ status: 'ERROR', msg: 'Erro ao Buscar Marca' });
            } else {
                notificationUtil.doNotify('err', 'Erro Interno do Servidor');
                res.status(400).send({ status: 'ERROR', msg: 'Erro Interno do Servidor' });
            }
        });
});
router.post('/', (req, res) => {
    const { body: { name } } = req;
    BrandModel.newBrand(name)
        .then(newBrandId => {
            if (newBrandId.stack) {
                notificationUtil.doNotify('err', 'Erro ao Salvar Marca');
                res.status(400).send({ status: 'ERROR', msg: 'Erro ao Salvar Marca' });
            } else {
                notificationUtil.doNotify('err', `Marca ${name} Salva com ID: ${newBrandId}`);
                res.status(200).send({
                    status: 'OK',
                    msg: `Marca ${name} Salva com ID: ${newBrandId}`
                });
            }
        })
        .catch(err => {
            if (err.stack) {
                notificationUtil.doNotify('err', 'Erro ao Salvar Marca');
                res.status(400).send({ status: 'ERROR', msg: 'Erro ao Salvar Marca' });
            } else {
                notificationUtil.doNotify('err', 'Erro Interno do Servidor');
                res.status(400).send({ status: 'ERROR', msg: 'Erro Interno do Servidor' });
            }
        });
});
router.put('/', (req, res) => {
    const { body: { id, name } } = req;
    BrandModel.editBrand(id, name)
        .then(brand => {
            if (brand.stack) {
                notificationUtil.doNotify('err', 'Erro ao Editar Marca');
                res.status(400).send({ status: 'ERROR', msg: 'Erro ao Editar Marca' });
            } else {
                notificationUtil.doNotify('err', `Marca ${name} Editada com Sucesso`);
                res.status(200).send(brand);
            }
        })
        .catch(err => {
            if (err.stack) {
                notificationUtil.doNotify('err', 'Erro ao Editar Marca');
                res.status(400).send({ status: 'ERROR', msg: 'Erro ao Editar Marca' });
            } else {
                notificationUtil.doNotify('err', 'Erro Interno do Servidor');
                res.status(400).send({ status: 'ERROR', msg: 'Erro Interno do Servidor' });
            }
        })

});
router.delete('/', (req, res) => {
    const { body: { id } } = req;
    BrandModel.deleteBrand(id)
        .then(data => {
            if (data.stack) {
                notificationUtil.doNotify('err', 'Um Dado Referenciando Impede a Exclusão');
                res.status(200).send({ status: 'WARNING', msg: 'Um Dado Referenciando Impede a Exclusão' });
            }
            else {
                notificationUtil.doNotify('err', 'Marca Deletada');
                res.status(200).send({ status: 'OK', msg: 'Marca Deletada' });
            }
        })
        .catch(err => {
            if (err.stack) {
                notificationUtil.doNotify('err', 'Erro ao Deletar Marca');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro ao Deletar Marca' });
            } else {
                notificationUtil.doNotify('err', 'Erro Interno do Servidor');
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Erro Interno do Servidor' });
            }
        })
});

module.exports = router;