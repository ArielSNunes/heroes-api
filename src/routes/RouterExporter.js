const express = require('express');
const AppRouter = express.Router();

const HeroRouter = require('./Hero');
const TableRouter = require('./Table');
const BrandRouter = require('./Brand');
const TeamRouter = require('./Team');

AppRouter.use('/hero', HeroRouter);
AppRouter.use('/table', TableRouter);
AppRouter.use('/brand', BrandRouter);
AppRouter.use('/team', TeamRouter);

module.exports = server => server.use(AppRouter);