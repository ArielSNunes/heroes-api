const express = require('express');
const router = express.Router();
const { TableModel } = require('../models/ModelExporter');

router.post('/', (req, res) => {
    const { body: { tableName } } = req;
    TableModel.getTableFieldNames(tableName)
        .then(fields => {
            if (!fields || fields.length <= 0)
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Error Retrieving Fields' });
            res.status(200).send(fields);
        })
        .catch(err => {
            if (err)
                res.status(400)
                    .send({ status: 'ERROR', msg: 'Error Retrieving Fields' });
            res.status(400)
                .send({ status: 'ERROR', msg: 'Internal Server Error' });
        });
});

module.exports = router;