const notifier = require('node-notifier');
const path = require('path');

const TITLE = {
    err: 'Ocorreu um Erro!',
    success: 'Operação Realizada com Sucesso'
};

const doNotify = (status, msg) => {
    if (
        status.toLowerCase() === 'err' ||
        status.toLowerCase() === 'success'
    ) {
        notifier.notify({
            title: TITLE[status],
            message: msg,
            icon: path.join(__dirname, 'assets', `${status.toLowerCase()}.png`),
            sound: false,
            wait: false
        }, (err) => {
            notifier.notify({
                title: 'Ocorreu um Erro: Erro nas Notificações Personalizadas',
                message: err
            })
        });
    }
}

module.exports = {
    doNotify
}