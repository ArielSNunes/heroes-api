const { env: {
    DB_CLIENT,
    DB_HOST,
    DB_PORT,
    DB_USER,
    DB_PW,
    DB_NAME
} } = process;

const knex = require('knex')({
    client: DB_CLIENT,
    connection: {
        host: DB_HOST,
        port: DB_PORT,
        user: DB_USER,
        password: DB_PW,
        database: DB_NAME
    }
});

module.exports = knex;