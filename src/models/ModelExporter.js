const HeroModel = require('./Hero');
const TableModel = require('./Table');
const BrandModel = require('./Brand');
const TeamModel = require('./Team');

module.exports = {
    HeroModel,
    TableModel,
    BrandModel,
    TeamModel
}