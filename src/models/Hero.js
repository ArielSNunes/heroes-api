const knex = require('../config/db');

const validateData = data => {
    return data !== undefined && data !== null;
};

const TABLE = {
    HERODATA: 'herodata',
    HERO: 'hero',
    STATUS: 'status'
};

const getHeroesList = () => {
    return knex(TABLE.HERODATA).select().orderBy('id')
        .then(heroList => heroList)
        .catch(err => err);
}
const getHero = heroId => {
    return knex(TABLE.HERODATA).select()
        .where('id', heroId).first()
        .then(hero => hero)
        .catch(err => err);
}
const newHeroStatus = status => {
    const { str, speed, int, res } = status;
    return knex(TABLE.STATUS).insert({ str, speed, int, res })
        .then(insertedStatusId => insertedStatusId)
        .catch(err => err);
}
const newHero = (heroInfo) => {
    const { personName, heroName, image, brand, status, team } = heroInfo;
    return newHeroStatus(status)
        .then(insertedStatusId => {
            if (!validateData(personName) ||
                !validateData(heroName) ||
                !validateData(image) ||
                !validateData(brand) ||
                !validateData(status) ||
                !validateData(team)) {
                return new Error('Missing Data');
            }
            if (insertedStatusId)
                return knex(TABLE.HERO).insert({
                    person_name: personName,
                    hero_name: heroName,
                    image: image,
                    brand: brand,
                    status: insertedStatusId,
                    team: team
                }).then(newHeroId => {
                    return newHeroId;
                }).catch(err => {
                    return err
                });
        }).then(newHeroId => newHeroId).catch(err => err);
}
const deleteHeroAndStatus = heroId => {
    return knex(TABLE.HERO).where({ id: heroId })
        .select('status').first()
        .then(statusInfo => {
            return knex(TABLE.HERO).where({ id: heroId })
                .delete()
                .then(deleteStatus => ({ deleteStatus, statusInfo }))
                .catch(err => err);
        })
        .then(({ deleteStatus, statusInfo }) => {
            if (deleteStatus === 1)
                return knex(TABLE.STATUS).where({ id: statusInfo.status })
                    .delete()
                    .then(deleteStatus => deleteStatus)
                    .catch(err => err);
            return new Error('Error Deleting Hero: ERROR='.concat(deleteStatus));
        })
        .then(deleteStatus => deleteStatus)
        .catch(err => err);
}

module.exports = {
    getHeroesList,
    getHero,
    newHero,
    deleteHeroAndStatus
}