const knex = require('../config/db');

const TABLE = 'team';

const getTeams = () => {
    return knex(TABLE).select()
        .then(teamList => teamList)
        .catch(err => err);
}
const getOneTeam = (teamId) => {
    return knex(TABLE).select()
        .where('id', teamId).first()
        .then(team => team)
        .catch(err => err);
}
const registerTeam = (teamName) => {
    return knex(TABLE).insert({ name: teamName })
        .then(newTeamId => newTeamId)
        .catch(err => err);
}
const editTeamName = (teamId, teamName) => {
    return knex(TABLE).where('id', teamId)
        .update({ name: teamName })
        .then(() => getOneTeam(teamId))
        .then(team => team)
        .catch(err => err);
}
const deleteTeam = (teamId) => {
    return knex(TABLE).where('id', teamId)
        .delete()
        .then(data => data)
        .catch(err => err);
}

module.exports = {
    getTeams,
    getOneTeam,
    registerTeam,
    editTeamName,
    deleteTeam
}