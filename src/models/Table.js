const knex = require('../config/db');

const getTableFieldNames = (tableName) => {
    return knex(tableName).columnInfo()
        .then(data => data)
        .catch(err => err);
}

module.exports = {
    getTableFieldNames
}