const knex = require('../config/db');

const TABLE = 'brand';

const getBrands = () => {
    return knex(TABLE).select()
        .then(brandList => brandList)
        .catch(err => err);
}

const getOneBrand = brandId => {
    return knex(TABLE).select()
        .where('id', brandId).first()
        .then(brand => brand)
        .catch(err => err);
}

const newBrand = brand => {
    return knex(TABLE).insert({ name: brand })
        .then(newBrandId => newBrandId)
        .catch(err => err);
}

const editBrand = (brandId, brandNewName) => {
    return knex(TABLE).where('id', brandId)
        .update({ name: brandNewName })
        .then(() => getOneBrand(brandId))
        .then(brand => brand)
        .catch(err => err);
}

const deleteBrand = (brandId) => {
    return knex(TABLE).where('id', brandId)
        .delete()
        .then(data => data)
        .catch(err => err);
}

module.exports = {
    getBrands,
    newBrand,
    editBrand,
    deleteBrand,
    getOneBrand
}