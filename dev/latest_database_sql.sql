-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema heroes
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema heroes
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `heroes` DEFAULT CHARACTER SET utf8 ;
USE `heroes` ;

-- -----------------------------------------------------
-- Table `heroes`.`brand`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `heroes`.`brand` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `heroes`.`status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `heroes`.`status` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `str` TINYINT(100) NULL DEFAULT 0,
  `speed` TINYINT(100) NULL DEFAULT 0,
  `int` TINYINT(100) NULL DEFAULT 0,
  `res` TINYINT(100) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 28
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `heroes`.`team`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `heroes`.`team` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `heroes`.`hero`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `heroes`.`hero` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `person_name` VARCHAR(100) NOT NULL,
  `hero_name` VARCHAR(50) NOT NULL,
  `image` VARCHAR(45) NOT NULL,
  `brand` INT(11) NOT NULL,
  `status` INT(11) NOT NULL,
  `team` INT(11) NULL DEFAULT NULL,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `brand_idx` (`brand` ASC),
  INDEX `status_idx` (`status` ASC),
  INDEX `team_idx` (`team` ASC),
  CONSTRAINT `brand`
    FOREIGN KEY (`brand`)
    REFERENCES `heroes`.`brand` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `status`
    FOREIGN KEY (`status`)
    REFERENCES `heroes`.`status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `team`
    FOREIGN KEY (`team`)
    REFERENCES `heroes`.`team` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 20
DEFAULT CHARACTER SET = utf8;

USE `heroes` ;

-- -----------------------------------------------------
-- Placeholder table for view `heroes`.`herodata`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `heroes`.`herodata` (`id` INT, `hero_name` INT, `person_name` INT, `image` INT, `str` INT, `speed` INT, `int` INT, `res` INT, `brand` INT, `team` INT);

-- -----------------------------------------------------
-- Placeholder table for view `heroes`.`herostatus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `heroes`.`herostatus` (`id` INT, `hero_name` INT, `person_name` INT, `image` INT, `str` INT, `speed` INT, `int` INT, `res` INT);

-- -----------------------------------------------------
-- View `heroes`.`herodata`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `heroes`.`herodata`;
USE `heroes`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`ArielSNunes`@`localhost` SQL SECURITY DEFINER VIEW `heroes`.`herodata` AS select `heroes`.`hero`.`id` AS `id`,`heroes`.`hero`.`hero_name` AS `hero_name`,`heroes`.`hero`.`person_name` AS `person_name`,`heroes`.`hero`.`image` AS `image`,`heroes`.`status`.`str` AS `str`,`heroes`.`status`.`speed` AS `speed`,`heroes`.`status`.`int` AS `int`,`heroes`.`status`.`res` AS `res`,`heroes`.`brand`.`name` AS `brand`,`heroes`.`team`.`name` AS `team` from (`heroes`.`team` left join (`heroes`.`brand` left join (`heroes`.`status` left join `heroes`.`hero` on(`heroes`.`status`.`id` = `heroes`.`hero`.`status`)) on(`heroes`.`brand`.`id` = `heroes`.`hero`.`brand`)) on(`heroes`.`team`.`id` = `heroes`.`hero`.`team`));

-- -----------------------------------------------------
-- View `heroes`.`herostatus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `heroes`.`herostatus`;
USE `heroes`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`ArielSNunes`@`localhost` SQL SECURITY DEFINER VIEW `heroes`.`herostatus` AS select `heroes`.`hero`.`id` AS `id`,`heroes`.`hero`.`hero_name` AS `hero_name`,`heroes`.`hero`.`person_name` AS `person_name`,`heroes`.`hero`.`image` AS `image`,`heroes`.`status`.`str` AS `str`,`heroes`.`status`.`speed` AS `speed`,`heroes`.`status`.`int` AS `int`,`heroes`.`status`.`res` AS `res` from (`heroes`.`status` left join `heroes`.`hero` on(`heroes`.`status`.`id` = `heroes`.`hero`.`status`));

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
